﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xeynergy.Challenge.Common.Dto;
using Xeynergy.Challenge.Data.Entities;
using Xeynergy.Challenge.Data.Repository;

namespace Xeynergy.Challenge.Core
{
    public interface IPersonBusinesEntity
    {
        void AddPerson(PersonDto personDto);
        void DeletePerson(int id);
        List<PersonDto> GetAll();
        PersonDto GetById(int id);
    }
    public class PersonBusinesEntity : IPersonBusinesEntity
    {
        private IGenericRepository<Person> personRepository;
        public PersonBusinesEntity(IGenericRepository<Person> personRepository)
        {
            this.personRepository = personRepository;
        }

        public void AddPerson(PersonDto personDto)
        {

            if (personDto.PersonType == Common.Enums.PersonType.Admin)
            {
                var admin = new Admin();
                admin.Privilege = personDto.Privilege;
                FillPersonEntity(personDto,admin);

                personRepository.Insert(admin);
            }

            else if (personDto.PersonType == Common.Enums.PersonType.User)
            {
                var user = new User();
                user.AttachedCustomerId = personDto.AttachedCustomerId;
                user.UserGroupId = personDto.UserGroupId;

                FillPersonEntity(personDto, user);
                personRepository.Insert(user);
            }

            personRepository.Save();
        }

        private void FillPersonEntity(PersonDto personDto, Person person)
        {
            person.EmailAddress = personDto.EmailAddress;
            person.FirstName = personDto.FirstName;
            person.LastName = personDto.LastName;
            person.Id = personDto.Id;
        }

        private PersonDto GetPersonDto(Person person)
        {
            var personDto= new PersonDto()
            {
                EmailAddress = person.EmailAddress,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Id = person.Id
            };

            if (person is Admin)
            {
                personDto.Privilege = ((Admin)person).Privilege;

            }

            else if(person is User)
            {
                var user = ((User)person);
                personDto.UserGroupId = user.UserGroupId;
                personDto.AttachedCustomerId = user.AttachedCustomerId;
            }

            return personDto;
        }

        public List<PersonDto> GetAll()
        {
            var users = personRepository.GetAll();

            
            var people= new List<PersonDto>();

            foreach (var item in users)
            {
                people.Add( GetPersonDto(item));
            }

            return people;

        }

        public void DeletePerson(int id)
        {
            throw new NotImplementedException();
        }

        public PersonDto GetById(int id)
        {
            return GetPersonDto(personRepository.GetById(id));
        }
    }
}
