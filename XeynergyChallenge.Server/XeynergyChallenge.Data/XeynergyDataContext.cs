﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xeynergy.Challenge.Data.Entities;

namespace Xeynergy.Challenge.Data
{
    public class XeynergyDataContext : DbContext
    {
        public XeynergyDataContext(DbContextOptions<XeynergyDataContext> options) : base(options)
        { 
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<AccessRule> AccessRules { get; set; }
    }
}

