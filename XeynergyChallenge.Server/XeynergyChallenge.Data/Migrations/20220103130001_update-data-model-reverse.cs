﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Xeynergy.Challenge.Data.Migrations
{
    public partial class updatedatamodelreverse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AttachedCustomerId",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "People",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Privilege",
                table: "People",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserGroupId",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_People_UserGroupId",
                table: "People",
                column: "UserGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_People_UserGroups_UserGroupId",
                table: "People",
                column: "UserGroupId",
                principalTable: "UserGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_People_UserGroups_UserGroupId",
                table: "People");

            migrationBuilder.DropIndex(
                name: "IX_People_UserGroupId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "AttachedCustomerId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "People");

            migrationBuilder.DropColumn(
                name: "Privilege",
                table: "People");

            migrationBuilder.DropColumn(
                name: "UserGroupId",
                table: "People");
        }
    }
}
