﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xeynergy.Challenge.Data.Entities;

namespace Xeynergy.Challenge.Data.Repository
{
    public class PersonRepository : GenericRepository<Person>
    {
        public PersonRepository(XeynergyDataContext dataContext):base(dataContext)
        {
        }
        
        public List<Admin> GetAdmins()
        {
            return base.context.People.OfType<Admin>().ToList();
        }
    }
}
