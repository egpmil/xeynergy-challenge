﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xeynergy.Challenge.Data.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        internal XeynergyDataContext context = null;
        private DbSet<T> entity = null;

        public GenericRepository(XeynergyDataContext context)
        {
            this.context = context;
            entity = context.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return entity.ToList();
        }
        public T GetById(object id)
        {
            return entity.Find(id);
        }
        public void Insert(T obj)
        {
            entity.Add(obj);
        }
        public void Update(T obj)
        {
            entity.Attach(obj);
            context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            T existing = entity.Find(id);
            entity.Remove(existing);
        }
        public void Save()
        {
            context.SaveChanges();
        }
    }
}
