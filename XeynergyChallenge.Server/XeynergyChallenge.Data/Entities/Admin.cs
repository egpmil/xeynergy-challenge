﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xeynergy.Challenge.Data.Entities
{
    public class Admin:Person
    {
        public string Privilege { get; set; }
    }
}
