﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xeynergy.Challenge.Data.Entities
{
    public class UserGroup : BaseEntity
    {
        public string GroupName { get; set; }

        public virtual ICollection<AccessRule> AccessRule { get; set; }
    }
}
