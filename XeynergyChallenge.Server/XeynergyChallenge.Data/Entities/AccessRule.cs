﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xeynergy.Challenge.Data.Entities
{
    public class AccessRule : BaseEntity
    {
        public string RuleName { get; set; }
        public bool Permission { get; set; }
    }
}
