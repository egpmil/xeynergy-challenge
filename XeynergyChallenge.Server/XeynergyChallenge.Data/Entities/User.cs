﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xeynergy.Challenge.Data.Entities
{
    public class User : Person
    {
        public int AttachedCustomerId { get; set; }

        public int UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
