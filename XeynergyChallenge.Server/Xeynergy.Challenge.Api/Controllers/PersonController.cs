﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xeynergy.Challenge.Common.Dto;
using Xeynergy.Challenge.Core;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Xeynergy.Challenge.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private IPersonBusinesEntity personBusinesEntity;
        public PersonController(IPersonBusinesEntity personEntity)
        {
            personBusinesEntity = personEntity;
        }
        // GET: api/<PersonController>
        [HttpGet]
        public IEnumerable<PersonDto> Get()
        {
            return personBusinesEntity.GetAll();
        }

        // GET api/<PersonController>/5
        [HttpGet("{id}")]
        public PersonDto Get(int id)
        {
            return personBusinesEntity.GetById(id);
        }

        // POST api/<PersonController>
        [HttpPost]
        public void Post([FromBody] PersonDto personDto)
        {
            personBusinesEntity.AddPerson(personDto);
        }

        // PUT api/<PersonController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PersonController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
