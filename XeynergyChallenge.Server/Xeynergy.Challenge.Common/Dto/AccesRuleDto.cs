﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xeynergy.Challenge.Common.Dto
{
    public class AccesRuleDto
    {
        public int Id { get; set; }
        public int RuleName { get; set; }
        public bool Permission { get; set; }
    }
}
