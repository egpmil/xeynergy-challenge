﻿using System;


using System.Collections.Generic;
using System.Text;
using static Xeynergy.Challenge.Common.Enums;

namespace Xeynergy.Challenge.Common.Dto
{
    public class PersonDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public PersonType PersonType { get; set; }
        public string Privilege { get; set; }
        public int AttachedCustomerId { get; set; }
        public int UserGroupId { get; set; }

    }
}
