﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xeynergy.Challenge.Common.Dto
{
    public class UserGroupDto
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }
}
