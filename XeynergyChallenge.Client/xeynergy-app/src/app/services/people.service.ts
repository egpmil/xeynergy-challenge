import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ViewPeopleItem } from '../view-people/view-people-datasource';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  getPeople(): Observable<ViewPeopleItem[]> {
    return this.http.get<ViewPeopleItem[]>(environment.baseUrl+'Person');
  }
}
