import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { PeopleService } from '../services/people.service';
import { ViewPeopleDataSource, ViewPeopleItem } from './view-people-datasource';

@Component({
  selector: 'app-view-people',
  templateUrl: './view-people.component.html',
  styleUrls: ['./view-people.component.css']
})
export class ViewPeopleComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<ViewPeopleItem>;
  dataSource: ViewPeopleDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered.
   * 
   *   id : number;
  firstName:string;
  lastName :string;
  emailAddress :string;
  personType : number;
  privilege :string;
  attachedCustomerId : number;
  userGroupId :number;
   */
  displayedColumns = ['id', 'firstName','lastName','emailAddress','personType'];

  constructor(private peopleService:PeopleService) {
    this.dataSource = new ViewPeopleDataSource([]);
  }

  ngAfterViewInit(): void {
  
  }

  ngOnInit(): void {
      this.peopleService.getPeople().subscribe((data: ViewPeopleItem[]) =>  
       {
          this.dataSource = new ViewPeopleDataSource(data);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.table.dataSource = this.dataSource;
      }
       );
  }
}
