import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPersonComponent } from './add-person/add-person.component';
import { ViewPeopleComponent } from './view-people/view-people.component';

const routes: Routes = [
{ path: 'view-people', component: ViewPeopleComponent },
{ path: 'add-people', component: AddPersonComponent },
{ path: '',   redirectTo: '/view-people', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
